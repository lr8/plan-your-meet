import './App.css';
import AppRouter from './components/AppRouter/AppRouter';

function App() {
  return (
    <div className="App">
      <AppRouter></AppRouter>
    </div>
  );
}

export default App;
