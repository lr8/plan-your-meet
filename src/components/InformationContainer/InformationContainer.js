import React, {useState} from 'react'
import "./InformationContainer.css"
import Arrow from "../../assets/arrow-left.png"
import { ClassMap } from 'class-map';
import ListElement from '../ListElement/ListElement';

export default function InformationContainer(props) {

    const [opened, setOpened] = useState(true);

    let renderedData;
    console.log(props);

    if(props.data.dates !== undefined && props.data.member !== undefined){
    if(props.type==="Mitglieder"){
        renderedData = props.data.member.map(
            (member, x) => {
                return(
                    <>
                        <ListElement key={x} data={member} type={props.type} state={props.state}></ListElement>
                    </>
                )
            }
        )
    }else if(props.type==="Veranstaltungen"){

        renderedData = props.data.dates.map(
            (date, x) => {
                return(
                    <>
                        <ListElement key={x} value={x} data={date} type={props.type} openedGroup={props.openedGroup}></ListElement>
                    </>
                )
            }
        )
    } 
    }else if(props.type==="Standort"){
        console.log(props.data.dates)
        renderedData = 
                <iframe
                    width="280px"
                    height="169px"
                    frameBorder="0" style={{border: "0"}}
                    src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyCXXVuHF6q1uoZwi7a0yrEUw3O95uvGB-E&q=${props.data.data.location}`} 
                    title="google map">
                </iframe>;
    }else if(props.type==="Aufgaben"){
        console.log(props.type); 
        renderedData = props.data.data.tasks.map(
            (task, x) => {
                return(
                    <ListElement key={x} value={x} data={task} type={props.type}></ListElement>
                )
            }
        )
    }
    var informationContainerClassMap = new ClassMap({
        "information-container": true,
        "opened-container": opened,
        "information-container--location": props.type==="Standort" && opened,
    })
    var informationContentContainerClassMap = new ClassMap({
        "information-container__content": true,
        "opened-container": opened,
    })
    var dropDownImageClassMap = new ClassMap({
        "dropdown-image": true,
        "arrow-up": opened,
    });

    const handleOnClick = () => {
        setOpened(!opened);
    }
   

    return (
        <div className={informationContainerClassMap}>
            <div className="information-container__headline">
                <h1>{props.type}: {props.type==="Standort" ? props.data.data.location : ""}</h1> 
                <button className="dropdown-button" onClick={(e)=> handleOnClick(e)}><img src={Arrow} className={dropDownImageClassMap} alt="dropdown"></img></button>
            </div>
            <div className={informationContentContainerClassMap}>
                {renderedData}
            </div>
    </div>
    )
}
