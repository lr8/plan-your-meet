import React from 'react'
import ListElement from '../ListElement/ListElement'

function DatesContainer(data) {
    
    console.log(data);
    const render = data.map((data, x)=>{
        return(
            <ListElement data={data} idx={x}></ListElement>
        )
    })
    
    return (
        <div>
            {render}
        </div>
    )
}

export default DatesContainer
