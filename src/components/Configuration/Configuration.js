import React, {useState} from 'react'
import "./Configuration.css"
import Hobbys from "../../assets/hobbys.png"
import Family from "../../assets/family.png"
import Friends from "../../assets/friends.png"
import  Working from "../../assets/working.png"




function Configuration({data}) {
    let renderedContent;
    
    /* group-states */
    const [icon, setIcon] = useState("friends");
    const [groupName, setGroupName] = useState("");
    
    /* other local states */
    
    /* current context */


    const handleOnClick = (e) => {
        if(data === "groups"){
        }
    }

    const setIconHandler = (e) => {
        e.preventDefault();
        if(e.target.className.includes("first")){
            setIcon("friends");
        }else if(e.target.className.includes("second")){
            setIcon("family");
        }else if(e.target.className.includes("third")){
            setIcon("hobbys");
        }else if(e.target.className.includes("fourth")){
            setIcon("work");
        }
    }
    const renderContent = () => {   
        if(data==="groups"){
            renderedContent = 
            <>
                <h2>Add a group</h2>
                <form className="configuration-form">
                    <input placeholder="Groupname" onChange={(e)=> setGroupName(e.target.value)} value={groupName} className="groupname-input"></input>
                    <p className="icon-headline">Group icon</p>
                    <div className="icon-selection">
                        <button className={`icon icon-first ${icon === "friends" ? "active-icon" : ""}`} onClick={(e)=> setIconHandler(e)}><img alt="friends" src={Friends} className="icon-image" type="button"></img>Friends</button>
                        <button className={`icon icon-second ${icon === "family" ? "active-icon" : ""}`} onClick={(e)=> setIconHandler(e)}><img alt="family" src={Family} className="icon-image" type="button"></img>Family</button>
                        <button className={`icon icon-third ${icon === "hobbys" ? "active-icon" : ""}`} onClick={(e)=> setIconHandler(e)}><img alt="hobbys" src={Hobbys} className="icon-image" type="button"></img>Hobby</button>
                        <button className={`icon icon-fourth ${icon === "work" ? "active-icon" : ""}`} onClick={(e)=> setIconHandler(e)}><img alt="work" src={Working} className="icon-image" type="button"></img>Work</button>
                    </div>
                    <button className="form-button" type="submit" onClick={(e)=> handleOnClick(e)}>Add group</button>
                </form>
            </>;

        }else if(data==="dates"){
            renderedContent = 
            <>
                <h2>Add a date</h2>
            </>;
        }else if(data==="tasks"){
            renderedContent = 
            <>
                <h2>Add a task</h2>
            </>;
        }else if(data==="profile"){
            renderedContent = 
            <>
            </>;
        }
    }
    renderContent();
    return (
        <div className="configuration">
            {renderedContent}
        </div>
    )
}

export default Configuration
