import React, {useState, useEffect} from 'react'
import "./ConfigureButton.css"
import {ClassMap} from "class-map";
import Configuration from '../Configuration/Configuration';

function ConfigureButton({data}) {
    const [isActive, setIsActive] = useState(false);
    let renderedContent;

    var buttonClassMap = new ClassMap({
        "configure-button": true,
        "active-button": isActive,
    })

    var containerClassMap = new ClassMap({
        "configure": true,
        "configure-groups": data === "groups",
        "configure-dates": data === "dates",
        "configure-tasks": data === "tasks",
        "configure-profile": data === "profile",
        "active-container" : isActive,
    })

    const renderContent = () => {   
        if(data==="groups"){
            renderedContent = 
            <>
                <button className={buttonClassMap} onClick={(e) => handleOnClick(e)}>+</button>
                <div className={containerClassMap}><Configuration data={data}></Configuration></div>
            </>;

        }else if(data==="dates"){
            renderedContent = 
            <>
                <button className={buttonClassMap} onClick={(e) => handleOnClick(e)}>+</button>
                <div className={containerClassMap}><Configuration data={data}></Configuration></div>
            </>;
        }else if(data==="tasks"){
            renderedContent = 
            <>
                <button className={buttonClassMap} onClick={(e) => handleOnClick(e)}>+</button>
                <div className={containerClassMap}><Configuration data={data}></Configuration></div>
            </>;
        }else if(data==="profile"){
            renderedContent = 
            <>
                <button className={buttonClassMap} onClick={(e) => handleOnClick(e)}>+</button>
                <div className={containerClassMap}><Configuration data={data}></Configuration></div>
            </>;
        }
    }
    renderContent();

    const handleOnClick = (e) => {
        setIsActive(!isActive);
    }

    useEffect(()=>{
        if(isActive === true){
            window.addEventListener("click", function handleOutsideClick(e){
                if(!this.document.querySelector(".config-position-helper").contains(e.target)){
                    setIsActive(false);
                    window.removeEventListener("click", handleOutsideClick);
                }   
            })
        }
    }, [isActive])
    
    return (
        <div className="config-position-helper">
            {renderedContent}
        </div>
    )
}

export default ConfigureButton
