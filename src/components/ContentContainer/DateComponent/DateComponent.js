import "./DateComponent.css"
import {ClassMap} from "class-map";
import Image from "../../../assets/x.png"
import InformationContainer from "../../InformationContainer/InformationContainer";




function DateComponent(props) {

    console.log(props);

    const handleOpenedClick = () => {
        
    console.log("Clicked Date Button");
        props.setOpenedDate(undefined);
    }

    var dateContainerClassMap = new ClassMap({
        "date-component": true,
        "is-opened-date": props.data.value === props.openedDate && props.openedGroup !== undefined,
    })
    var closeButtonClassMap = new ClassMap({
        "close-button-date": true,
        "is-opened-button-date": props.data.value === props.openedDate && props.openedGroup !== undefined,
    })

    return (
        <>
         <div className={dateContainerClassMap}> 
            <h1 className="date-headline">{props.data.data.datename}</h1>
            <p className="date-creator">Ersteller: {props.data.data.creator}</p>
            <p className="date-date">Datum: {props.data.data.time}</p>
            <p className="date-participation-question">Nimmst du an der Veranstaltung teil?</p>
            <div className="survey-container">
                <button className="survey-button-yes">Ja</button>
                <button className="survey-button-no">Nein</button>

            </div>
            <InformationContainer data={props.data} type="Standort"></InformationContainer>
            <InformationContainer data={props.data} type="Aufgaben"></InformationContainer>
        </div>
        <button className={closeButtonClassMap} onClick={()=>handleOpenedClick()}><img alt="close" src={Image} class="close-button__image-date"></img></button> 
        </>
    )
}

export default DateComponent
