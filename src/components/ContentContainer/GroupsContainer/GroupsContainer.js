import React, {useState} from 'react'
import Hobbys from "../../../assets/hobbys.png"
import Family from "../../../assets/family.png"
import Friends from "../../../assets/friends.png"
import  Working from "../../../assets/working.png"
import "./GroupsContainer.css"
import GroupComponent from './GroupComponent/GroupComponent'
import Context from "../../GroupContext/GroupContext";

function GroupsContainer() {

    const [openedGroup, setOpenedGroup] = useState(undefined);
    const [data, setData] = useState([
            {
                groupname: "Gruppenname1",
                icon: Friends,
                creator: "You",
                member: [
                    "You",
                    "Peter Hans Sachs",
                    "Jürgen Rösler",
                    "Susanne Horn",
                    "Klaus Schmidt",
                ],
                dates: [
                    {
                        datename: "Veranstaltung1",
                        location: "Berlin, Germany",
                        creator: "Max Mustermann",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    },
                    {
                        datename: "Veranstaltung2",
                        location: "Berlin, Germany",
                        creator: "Max Mustermann",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    }
                ],
            },
            {
                groupname: "Gruppenname2",
                icon: Family,
                creator: "Someone elses else",
                member: [
                    "You",
                    "Peter Hans Sachs",
                    "Jürgen Rösler",
                    "Susanne Horn",
                    "Klaus Schmidt",
                ],
                dates: [
                    {
                        datename: "Veranstaltung1",
                        creator: "Max Mustermann",
                        location: "Berlin, Germany",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    },
                    {
                        datename: "Veranstaltung2",
                        location: "Berlin, Germany",
                        creator: "Max Mustermann",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    }
                ],
            },
            {
                groupname: "Gruppenname3",
                icon: Working,
                creator: "Employee",
                member: [
                    "You",
                    "Peter Hans Sachs",
                    "Jürgen Rösler",
                    "Susanne Horn",
                    "Klaus Schmidt",
                ],
                dates: [
                    {
                        datename: "Veranstaltung1",
                        creator: "Max Mustermann",
                        location: "Berlin, Germany",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    },
                    {
                        datename: "Veranstaltung2",
                        location: "Berlin, Germany",
                        creator: "Max Mustermann",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    }
                ],
            },
            {
                groupname: "Gruppenname4",
                icon: Working,
                creator: "Employee",
                member: [
                    "You",
                    "Peter Hans Sachs",
                    "Jürgen Rösler",
                    "Susanne Horn",
                    "Klaus Schmidt",
                ],
                dates: [
                    {
                        datename: "Veranstaltung1",
                        creator: "Max Mustermann",
                        location: "Frankfurt, Germany",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    },
                    {
                        datename: "Veranstaltung2",
                        location: "Berlin, Germany",
                        creator: "Max Mustermann",
                        weather: "13°C",
                        time: "23.07.21 13:00",
                        accept: [
                            "You",
                            "Peter Hans Sachs",
                            "Jürgen Rösler",
                        ],
                        decline: [
                            "Klaus Schmidt",            
                        ],
                        notVoted: [
                            "Susanne Horn",
                        ],
                        tasks: [
                            {task: "Task1", responsible: "Peter Hans Sachs"},
                            {task: "Task2", responsible: "Susanne Horn"},
                            {task: "Task3", responsible: "Klaus Schmidt"},
                            {task: "Task4", responsible: "Peter Hans Sachs"},
                        ],
                    }
                ],
            },
        ]);
    
        const renderedData = data.map(
            (renderedData, x) => {
                return(
                    <div key={x}>
                    <div className={`group-item group-item${x}`} onClick={(e)=> handleOnClick(e)}>
                        <img src={renderedData.icon} alt="img" className="group-item__image"></img>
                        <div className="group-item__info">
                            <h6 className="group-item__heading">{renderedData.groupname}</h6>
                            <p className="group-item__creator">{renderedData.creator}</p>
                        </div>
                    </div>
                    <hr className="hr"></hr>
                    <Context.Provider value={data[x]}>
                    <GroupComponent openedGroup={openedGroup} value={x} setOpenedGroup={setOpenedGroup}></GroupComponent>
                    </Context.Provider>
                    </div>
                )
            }
        )

    const handleOnClick = (e) => {
        let activeElement = e.target.classList[1];
        let activeElementId = activeElement.charAt(activeElement.length-1)
        setOpenedGroup(activeElementId);
        document.querySelector(".configure-button").classList.add("disabled-button");
    }

    return (
        <div className="groups-container">
            {renderedData}
        </div>
    )
}

export default GroupsContainer
