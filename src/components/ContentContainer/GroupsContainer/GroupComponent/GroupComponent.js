import React, {useState , useContext} from 'react'
import Context from "../../../GroupContext/GroupContext";
import "./GroupComponent.css"
import {ClassMap} from "class-map";
import Arrow from "../../../../assets/arrow-left.png"
import InformationContainer from '../../../InformationContainer/InformationContainer';

function GroupComponent(props) {

    const [data, setData] = useState(useContext(Context));

    var groupContainerClassMap = new ClassMap({
        "group-component": true,
        "is-opened-group": props.value.toString() === props.openedGroup,
    })
    var closeButtonClassMap = new ClassMap({
        "close-button": true,
        "is-opened-button": props.value.toString() === props.openedGroup,
    })

    const handleOnClickClose = ()=> {
        console.log("ClickedGroupClose");
        props.setOpenedGroup(undefined);
        document.querySelector(".configure-button").classList.remove("disabled-button");
    }

    return (
        <>
            <div className={`${groupContainerClassMap}`}>
                <img src={data.icon} alt="icon" className="group-image"></img>
                <h1 className="group-headline">{data.groupname}</h1>
                <p className="group-creator">Ersteller: {data.creator}</p>
                <div className="information-section">
                    <InformationContainer data={data} type="Mitglieder"></InformationContainer>
                    <InformationContainer data={data} type="Veranstaltungen" openedGroup={props.openedGroup}></InformationContainer>
                </div>

            </div>
            <button className={`${closeButtonClassMap}`} onClick={handleOnClickClose}><img src={Arrow} alt="arrow-left" className="close-button__image"></img></button>
        </>
    )
}

export default GroupComponent
