import React from 'react'
import DatesContainer from '../DatesContainer/DatesContainer';
import "./ContentContainer.css"
import GroupsContainer from './GroupsContainer/GroupsContainer';

function ContentContainer({data}) {
    let renderedContent;

    const renderContent = () => {
        if(data==="groups"){
            renderedContent = <GroupsContainer data={data}></GroupsContainer>;
        }else if(data==="dates"){
            renderedContent = <p>sss</p>;
        }else if(data==="tasks"){
            renderedContent = <p>tasks</p>;
        }else if(data==="profile"){
            renderedContent = <p>profile</p>;
        }
    }
    renderContent();
    return (
        <div className="content-container">
            {renderedContent}
        </div>
    )
}

export default ContentContainer
