import React, {useState} from 'react';
import "./ListElement.css";
import X from "../../assets/x.png";
import Arrow from "../../assets/arrow-left.png"
import { ClassMap } from 'class-map';
import DateComponent from '../ContentContainer/DateComponent/DateComponent';
import Checkmark from "../../assets/checkmark.png"

function ListElement(data) {

    const [isFinished, setIsFinished] = useState(false);
    const [openedDate, setOpenedDate] = useState(undefined);

    let renderedData;
    var doneTask = new ClassMap({
        "list-element__button--task": true,
        "done-task": isFinished,
    })

    const handleOnClickTask = (e) => {
        console.log(data.type)
        if(data.type==="Aufgaben"){
            setIsFinished(!isFinished);
        }else if(data.type ==="Mitglieder"){
            console.log("Mitglieder");
        }
    }

    const renderContent = ()=> {

    const handleOnClick= (e) => {
        console.log(data);
        setOpenedDate(data.value);
    } 

        if(data.type==="Mitglieder"){
            renderedData = 
            <>
                <div className={`list-element`}>
                    <div className="list-element__content">
                    <p>{data.data}</p>
                    </div>
                    <button className="list-element__button--member">
                        {data.data==="You" ? "" : <img className="list-element__button--image" src={X} alt="x"></img>}
                    </button>
                </div>
                <hr></hr>
            </>           
            ;
        }else if(data.type==="Veranstaltungen"){
            renderedData =
                <>
                    <div className={`list-element list-element${data.value} list-element__dates`} onClick={(e) => handleOnClick(e)}>
                        <div className="list-element__content">
                            <p className="list-element__content-date">{data.data.datename}</p>
                            <p className="list-element__content-time">{data.data.time}</p>
                        </div>
                            <img className="list-element__image" src={Arrow} alt="x"></img>
                    </div>
                    <hr></hr>
                    <DateComponent data={data} openedDate={openedDate} setOpenedDate={setOpenedDate} openedGroup={data.openedGroup}></DateComponent>
                </>           
            ;
        }else if(data.type==="Aufgaben"){
            renderedData = 
            <>
                <div className={`list-element`}>
                    <div className="list-element__content">
                        <p className="list-element__content-task">{data.data.task}</p>
                        <p className="list-element__content-responsible">{data.data.responsible}</p>
                    </div>
                    <div className="list-element__button-container">
                        <button className={doneTask} onClick={(e)=> handleOnClickTask(e)}>
                            <img src={Checkmark} alt="checkmark" className="list-element__button--image"></img>
                        </button>
                    </div>
                </div>
            </>
        }
    }
    renderContent();

    
    return (
        <>
            {renderedData}
        </>
    )
}

export default ListElement
