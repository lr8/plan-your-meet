import React, {useState} from 'react';
import ConfigureButton from '../ConfigureButton/ConfigureButton';
import ContentContainer from '../ContentContainer/ContentContainer';
import "./AppRouter.css";
import ProfilePicture from "../../assets/profile.png";
import GroupsPicture from "../../assets/groups.png";
import TasksPicture from "../../assets/tasks.png";
import CalendarPicture from "../../assets/calendar.png";

function AppRouter() {

    const [content, setContent] = useState("groups");

    const handleOnClick = (e) => {
        if(e.target.className.includes("groups")){
            setContent("groups");
        }else if(e.target.className.includes("dates")){
            setContent("dates");
        }else if(e.target.className.includes("configure")){
            console.log("Mach später")
        }else if(e.target.className.includes("tasks")){
            setContent("tasks");
        }else if(e.target.className.includes("profile")){
            setContent("profile");
        }
        document.querySelector(".configure-button").classList.remove("disabled-button");
    }

    return (
        <div className="router">
            <div className="topbar">plan your meet_</div>
            <div className="content">
                <ContentContainer data={content}></ContentContainer>
            </div>
            <div className="controlbar">
                <div className="controlbar--inner">
                <button className="button button--groups" onClick={(e)=> handleOnClick(e)}><img alt="groups" src={GroupsPicture} className="button-image"/></button>
                <button className="button button--dates" onClick={(e)=> handleOnClick(e)}><img alt="groups" src={CalendarPicture} className="button-image"/></button>
                <span className="central-button--helper">
                    <ConfigureButton data={content}></ConfigureButton>
                </span>
                <button className="button button--tasks" onClick={(e)=> handleOnClick(e)}><img alt="groups" src={TasksPicture} className="button-image"/></button>
                <button className="button button--profile" onClick={(e)=> handleOnClick(e)}><img alt="groups" src={ProfilePicture} className="button-image"/></button>
                </div>
            </div>
        </div>
    )
}

export default AppRouter
